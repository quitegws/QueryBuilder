<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>QueryBuilder-查询结果</title>
<link rel="shortcut icon" href="<%=path %>/css/images/ico.ico" type="image/x-icon" /> 
<link href="<%=path%>/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
	
<!-- 公共的样式 -->
<link href="<%=path%>/css/base.css" rel="stylesheet" type="text/css" />

<!-- 警告框样式 -->
<link href="<%=path%>/css/sweetalert.css" rel="stylesheet"
	type="text/css" />
<!-- 公共的css样式 -->
<link href="<%=path%>/css/currency.css" rel="stylesheet" type="text/css" />

<!-- 警告框js -->
<script src="<%=path%>/js/sweetalert.min.js"></script>

<script type="text/javascript" src="<%=path%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=path%>/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.validate.min.js"></script>
</head>

<body>
	<ul class="nav nav-tabs" id="query_ul">
		<li role="presentation" id="query_template" ><a
			href="<%=path %>/jumpDemo">模板管理</a></li>
		<li role="presentation" id="builder_query"><a
			href="<%=path %>/builderQuery">查询结构管理</a></li>
		<li role="presentation" id="query_result" class="active"><a
			href="<%=path %>/queryResult">查询结果管理</a></li>
	</ul>
	<div class="dis_pro">
		<input type="hidden" id="path" value="<%=path%>" /> <input
			type="hidden" id="jsPath" value="www.niubai.net.cn" /> <input
			type="hidden" id="option" /> <input type="hidden" id="opRow" />
	</div>

	<!--footer-->
	<div class="jq_foot">
		<span class="mr30">版权所有 © 2016</span>QueryBuilder
	</div>
</body>
</html>